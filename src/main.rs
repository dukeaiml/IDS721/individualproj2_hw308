use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct NumbersInput {
    numbers: Vec<u32>,
}

#[derive(Serialize)]
struct PrimeNumbersResponse {
    primes: Vec<u32>,
}

fn is_prime(n: u32) -> bool {
    if n <= 1 {
        return false;
    }
    for i in 2..=(n as f64).sqrt() as u32 {
        if n % i == 0 {
            return false;
        }
    }
    true
}

async fn identify_primes(input: web::Json<NumbersInput>) -> impl Responder {
    let primes: Vec<u32> = input
        .numbers
        .iter()
        .cloned()
        .filter(|&n| is_prime(n))
        .collect();

    HttpResponse::Ok().json(PrimeNumbersResponse { primes })
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new().route("/identify_primes", web::post().to(identify_primes))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}


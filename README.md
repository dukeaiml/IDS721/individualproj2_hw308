# individualproj2_hw308
This project created a Rust web service using Actix-Web that identifies all prime numbers from a given list of integers. The service will accept a JSON payload containing a list of integers and respond with a list of numbers from that list which are prime.


## Getting started

Continuous Delivery of Rust Microservice

Requirements:
1. Simple REST API/web service in Rust
2. Dockerfile to containerize service
3. CI/CD pipeline files

## Instructions

Step 1: Code part
- Create a new cargo project with `cargo new individualproj2`
- Add corresponding dependencies in the file `Cargo.toml`. 
- Add Rust code for identifying primes in the file `main.rs`.
- Build `Dockerfile` to build image for Docker
- Build `.gitlab-ci.yml` file for deployment on gitlab.

Step 2: Running part

- Build the image based on the command in Dockerfile, using `sudo docker build -t individualproj2 .`
- Build a container based on the image and run the container locally, which can be accessed on the 8088 port, `sudo docker run -p 8080:8080 individualproj2 .`
- JSON input list of numbers for testing `curl -X POST http://localhost:8080/identify_primes -H "Content-Type: application/json" -d '{"numbers":[1, 2, 3, 4, 5, 7, 11, 13, 17, 19, 23, 29]}'`
`

## Screenshots for Requirements

- **Rust Microservice Functionality**

**Code for main.rs**
![](images/main.png)

- **Docker Configuration**

**Image built**
![](images/docker2_build.png)

**Container built**
![](images/container.png)

**Testing Ouput returns the prime numbers in the input list**
![](images/output.png)


- **CI/CD Pipeline**

**Pipeline passed**
![](images/pipeline.png)

- **Demo Video**

The video `demo_video.webm` is in the root directory of individualproj2_hw308 above, it shows the complete testing process under the running container.

